execute pathogen#infect()

set cursorline
set number
set tabstop=4
set softtabstop=4
set shiftwidth=4

set colorcolumn=80
highlight ColorColumn ctermbg=darkgray
	
set background=dark
let base16colorspace=256
colorscheme base16-default-dark
