#!/bin/sh
#
# Set up a basic environment for XFCE4 desktop on Debian

deps="zsh vim"
files=".zshrc .zshrc.local .vimrc .zshenv .aliases"
dir=~/dotfiles
olddir=~/.dotfiles_backup

# Install packages
sudo apt-get install $deps

# Change default shell
chsh -s $(which zsh) $USER

# Download grml zsh config
wget -O .zshrc http://git.grml.org/f/grml-etc-core/etc/zsh/zshrc

# SYMLINK FILES ================================================================
#
# Make a backup directory for existing files
mkdir -p $olddir
cd $dir

# Move existing files to backup-directory, symlink new files
for file in $files; do
	mv ~/$file $olddir
	ln -s $dir/$file ~/$file
done

# SHELL ========================================================================
#
# Get shell colorschemes
git clone https://github.com/chriskempson/base16-shell.git \
	~/.config/base16-shell

# Get xfce4-terminal colorschemes
git clone https://github.com/chriskempson/base16-xfce4-terminal \
	~/.config/base16-xfce4-terminal

cat ~/.config/base16-xfce4-terminal/base16-default.dark.terminalrc \
> ~/.config/xfce4/terminal/terminalrc

# Get zsh plugins
mkdir -p ~/.zshrc.d/plugins
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git \
	~/.zshrc.d/plugins/zsh-syntax-highlighting

# VIM ==========================================================================
#
# Install pathogen
mkdir -p ~/.vim/autoload ~/.vim/bundle
wget -O ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

# Install plugins
git clone git://github.com/tpope/vim-sensible.git ~/.vim/bundle/vim-sensible
git clone https://github.com/chriskempson/base16-vim.git ~/.vim/bundle/base16-vim
